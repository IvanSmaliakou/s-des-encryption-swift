//
//  AppDelegate.swift
//  lab3_DES
//
//  Created by Ivan Smaliakou on 20.11.20.
//  Copyright © 2020 Ivan Smaliakou. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}
