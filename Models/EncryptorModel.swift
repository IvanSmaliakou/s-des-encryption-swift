//
//  EncryptorModel.swift
//  lab3_DES
//
//  Created by Ivan Smaliakou on 23.11.20.
//  Copyright © 2020 Ivan Smaliakou. All rights reserved.
//

import Foundation

protocol EncryptorModelDelegate: class{
    func EncryptedTextDidChange();
}

class EncryptorModel {
    weak var delegate: EncryptorModelDelegate?;
    var FP: Array<Int> = [4,1,3,5,7,2,8,6];
    var ExitP: Array<Int> = [2,4,3,1];
    var SLeftMatrix: [[Int]] = [
        [1,0,3,2],
        [3,2,1,0],
        [0,2,1,3],
        [3,1,3,1]
    ];
    var SRightMatrix: [[Int]] = [
        [1,1,2,3],
        [2,0,1,3],
        [3,0,1,0],
        [2,1,0,3]
    ];
    var EP: Array<Int> = [4,1,2,3,2,3,4,1];
    var IP: Array<Int> = [2,6,3,1,4,8,5,7];
    var P8: Array<Int> = [6,3,7,4,8,5,10,9];
    var P10: Array<Int> = [3,5,2,7,4,10,1,9,8,6];
        
    var EncryptedText: Character?{
        didSet{
            delegate?.EncryptedTextDidChange();
        }
    }
}
