//
//  ServiceUtils.swift
//  lab3_DES
//
//  Created by Ivan Smaliakou on 26.11.20.
//  Copyright © 2020 Ivan Smaliakou. All rights reserved.
//

import Foundation


func toBinArr(n: Int32) -> [Int]{
    var capturedN  = n;
    var ret = [Int]();
    while (capturedN > 0) {
        ret.append(Int(capturedN % 2));
        capturedN /= 2;
    }
    return ret.reversed();
}

func toDec(n: [Int]) -> Int{
    let nR = n.reversed() as [Int];
    var res: Int = 0;
    for i in 0...nR.count-1 {
        res += nR[i] * Int(pow(Double(2), Double(i)));
    }
    return res;
}

func lShift(arr: Array<Int>, n: Int) -> Array<Int>{
    var ret = Array(repeating: 0, count: arr.count);
    for i in 0...arr.count-n-1 {
        ret[i] = arr[i+n];
    }
    var j = 0;
    for i in arr.count-n...arr.count-1{
        ret[i] = arr[j];
        j+=1;
    }
    return ret;
}

func permutate(arr: [Int], pTable: [Int]) -> [Int]{
    var resArr = [Int]();
    for resArrIndex in pTable{
        resArr.append(arr[resArrIndex-1]);
        }
    return resArr;
}

func XOR(a: [Int], b: [Int]) -> [Int] {
    let diff = a.count>b.count ? a.count-b.count : b.count-a.count;
    var na = a;
    var nb = b;
    if a.count > b.count{
        nb+=Array(repeating: 0, count: diff);
    }
    if b.count > a.count{
        na+=Array(repeating: 0, count: diff);
    }
    var ret = [Int]();
    for i in 0...na.count-1 {
        ret.append(na[i] ^ nb[i]);
    }
    return ret;
}

func NumberFromSMatrix(matrix: [[Int]], input: [Int]) -> Int {
    let row = toDec(n: [input[0]] + [input[3]]);
    let column = toDec(n: [input[1]] + [input[2]]);
    
    return matrix[row][column];
}

func binToString(arr: [Int]) -> String{
    var res = "";
    for el in arr {
        res.append(String(el));
    }
    return res;
}

func arrToString(arr: [Int]) -> String {
    var ret = "";
    arr.forEach{ el in
        ret.append(String(el));
    }
    return ret;
}
func pad(arr: [Int], toLen: Int) -> [Int]{
    let diff = toLen - arr.count;
    if diff <= 0{
        return arr;
    }
    return Array(repeating: 0, count: diff) + arr;
}

extension Character{
    func binArr()-> [Int]{
        return toBinArr(n: Int32(self.unicodeScalars[self.unicodeScalars.startIndex].value));
    }
}
