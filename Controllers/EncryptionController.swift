//
//  EncryptionController.swift
//  lab3_DES
//
//  Created by Ivan Smaliakou on 23.11.20.
//  Copyright © 2020 Ivan Smaliakou. All rights reserved.
//

import Foundation

class EncryptionController: EncryptorModelDelegate{
    var viewController:  ViewController;
    var model: EncryptorModel;
    
    init(m: EncryptorModel, vc: ViewController) {
        model = m;
        viewController = vc;
    };
    
    func Encrypt(symbol: Character, key: String){
        let keyInt = (key as NSString).intValue;
        let (subkey1, subkey2) = subkeys(keyInt: keyInt);
        
        let binSymbol = pad(arr: symbol.binArr(), toLen: 8);
        let initPermutated = permutate(arr: binSymbol, pTable: model.IP);
        let binSymbolLeft = [Int](initPermutated.prefix(initPermutated.count/2));
        let binSymbolRight = [Int](initPermutated.dropFirst(initPermutated.count - binSymbolLeft.count));

        let FkRes = Fk(subkey: subkey1, binSymbolRight: binSymbolRight, binSymbolLeft: binSymbolLeft, true);
        
        let (swapBitsLeft, swapBitsRight) = swapBits(FkRes, binSymbolRight);
        let op4Res = Fk(subkey: subkey2, binSymbolRight: swapBitsRight, binSymbolLeft: swapBitsLeft, false);
        let extendedOp4Res = op4Res + swapBitsRight;
        let finishingPermutationRes = permutate(arr: extendedOp4Res, pTable: model.FP);
        let decFinishingPermutationRes = toDec(n: finishingPermutationRes);
        let str = String(Unicode.Scalar(decFinishingPermutationRes)!);
        self.viewController.key1TextBox.stringValue = arrToString(arr: subkey1);
        self.viewController.key2TextBox.stringValue = arrToString(arr: subkey2);
        self.viewController.afterInitPermutationBox.stringValue = arrToString(arr: initPermutated);
        self.viewController.afterSwapBox.stringValue = arrToString(arr: swapBitsLeft + swapBitsRight);
        self.viewController.aftefFkBox2.stringValue = arrToString(arr: extendedOp4Res);
        self.viewController.binSymbol.stringValue = arrToString(arr: finishingPermutationRes);
        self.viewController.resultBox.stringValue = str;
    }
    
    func Decrypt(symbol: Character, key: String){
        let keyInt = (key as NSString).intValue;
        let (subkey1, subkey2) = subkeys(keyInt: keyInt);
        
        let binSymbol = pad(arr: symbol.binArr(), toLen: 8);
        let initPermutated = permutate(arr: binSymbol, pTable: model.IP);
        let binSymbolLeft = [Int](initPermutated.prefix(initPermutated.count/2));
        let binSymbolRight = [Int](initPermutated.dropFirst(initPermutated.count - binSymbolLeft.count));

        let FkRes = Fk(subkey: subkey2, binSymbolRight: binSymbolRight, binSymbolLeft: binSymbolLeft, true);
        
        let (swapBitsLeft, swapBitsRight) = swapBits(FkRes, binSymbolRight);
        let op4Res = Fk(subkey: subkey1, binSymbolRight: swapBitsRight, binSymbolLeft: swapBitsLeft, false);
        let finishingPermutationRes = permutate(arr: op4Res + swapBitsRight, pTable: model.FP);
        let decFinishingPermutationRes = toDec(n: finishingPermutationRes);
        let str = String(Unicode.Scalar(decFinishingPermutationRes)!);
        self.viewController.key1TextBox.stringValue = arrToString(arr: subkey1);
        self.viewController.key2TextBox.stringValue = arrToString(arr: subkey2);
        self.viewController.afterInitPermutationBox.stringValue = arrToString(arr: initPermutated);
        self.viewController.afterSwapBox.stringValue = arrToString(arr: swapBitsLeft + swapBitsRight);
        self.viewController.aftefFkBox2.stringValue = arrToString(arr: op4Res + swapBitsRight);
        self.viewController.binSymbol.stringValue = arrToString(arr: finishingPermutationRes);
        self.viewController.resultBox.stringValue = str;
    }
    
    func swapBits(_ a:[Int], _ b:[Int]) -> ([Int], [Int]) {
        return (b, a);
    }
    func EncryptedTextDidChange(){
        viewController.resultBox.stringValue = String(((model.EncryptedText ?? "e".last)!));
    }
    
    func subkeys(keyInt: Int32) -> ([Int], [Int]) {
        let binArr: Array<Int> = pad(arr: toBinArr(n: keyInt), toLen: 10);
        
        let permutationArr: Array<Int> = model.P10;
        
        let half1 = permutationArr.prefix(permutationArr.count / 2);
        let half2 = permutationArr.dropFirst(permutationArr.count - half1.count);
        let shifted1 = lShift(arr: Array(half1), n: 1);
        let shifted2 = lShift(arr: Array(half2), n: 1);
        let shiftedArr1 = shifted1 + shifted2;
        let subKey1Ptable = permutate(arr: shiftedArr1, pTable: model.P8);
        let subKey1 = permutate(arr: binArr, pTable: subKey1Ptable);
        let shiftedSecond1 = lShift(arr: shifted1, n: 2);
        let shiftedSecond2 = lShift(arr: shifted2, n: 2);
        let shiftedArr2 = shiftedSecond1 + shiftedSecond2;
        let subKey1PtableSecond = permutate(arr: shiftedArr2, pTable: model.P8);
        let subKey2 = permutate(arr: binArr, pTable: subKey1PtableSecond);
        return (subKey1, subKey2);
    }
    
    func Fk(subkey: [Int], binSymbolRight: [Int], binSymbolLeft: [Int], _ isFirst: Bool) -> [Int] {
       let extendedSymbolRight = permutate(arr: [Int](binSymbolRight), pTable: model.EP);
       let xoredWithKey = XOR(a: extendedSymbolRight, b: subkey);
       
       let xoredLeft = xoredWithKey.prefix(xoredWithKey.count/2);
       let xoredRight = xoredWithKey.dropFirst(xoredWithKey.count - xoredLeft.count);
       let SL = NumberFromSMatrix(matrix: model.SLeftMatrix, input: [Int](xoredLeft));
       let SR = NumberFromSMatrix(matrix: model.SRightMatrix, input: [Int](xoredRight));
       let SArr = pad(arr: toBinArr(n: Int32(SL)), toLen: 2) + pad(arr: toBinArr(n: Int32(SR)), toLen: 2);
       let permutatedSArr = permutate(arr: SArr, pTable: model.ExitP);
       let xoredPermutatedSArr = XOR(a: permutatedSArr, b: [Int](binSymbolLeft));
       if (isFirst){
           self.viewController.afterEPBox.stringValue = arrToString(arr: extendedSymbolRight);
           self.viewController.afterXOREPk1Box.stringValue = arrToString(arr: xoredWithKey);
           self.viewController.afterSmatrixBox.stringValue = arrToString(arr: SArr);
           self.viewController.afterP4Box.stringValue = arrToString(arr: permutatedSArr);
           self.viewController.afterXORLP4Box.stringValue = arrToString(arr: xoredPermutatedSArr);
       }
       return xoredPermutatedSArr;
    }
}
