//
//  ViewController.swift
//  lab3_DES
//
//  Created by Ivan Smaliakou on 20.11.20.
//  Copyright © 2020 Ivan Smaliakou. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSTextFieldDelegate {
    
    var controller: EncryptionController?;
    var model: EncryptorModel?;
    
    @IBOutlet weak var resultBox: NSTextField!
    
    weak var inputKeyTextBox: NSTextField!
    
    @IBOutlet weak var inputTextBox: NSTextField!
    
    @IBOutlet weak var key1TextBox: NSTextField!
    
    @IBOutlet weak var key2TextBox: NSTextField!
    
    @IBOutlet weak var binSymbol: NSTextField!
    
    @IBOutlet weak var aftefFkBox2: NSTextField!
        
    @IBOutlet weak var afterXOREPk1Box: NSTextField!
    
    @IBOutlet weak var afterEPBox: NSTextField!
    
    @IBOutlet weak var afterSmatrixBox: NSTextField!
    
    @IBOutlet weak var afterFinishingPermutationBox: NSTextField!
    
    @IBOutlet weak var afterSwapBox: NSTextField!
    
    @IBOutlet weak var afterXORLP4Box: NSTextField!
    
    @IBOutlet weak var afterP4Box: NSTextField!
    
    @IBOutlet weak var afterInitPermutationBox: NSTextField!
    
    @IBOutlet weak var decryptButtonOutlet: NSButton!
    
    @IBOutlet weak var encryptButtonOutlet: NSButton!
    
    @IBAction func decryptButtonPushed(_ sender: Any) {
        controller!.Decrypt(symbol: Character(inputTextBox.stringValue), key: inputKeyTextBox.stringValue);
    }
    @IBAction func encryptButtonPushed(_ sender: Any) {
        controller!.Encrypt(symbol: Character(inputTextBox.stringValue), key: inputKeyTextBox.stringValue);
    }
    public func controlTextDidChange(_ _: Notification) {
        var buttonsActive = true;
        if ( (inputTextBox.stringValue.count <= 0 || inputKeyTextBox.stringValue.count <= 0) && buttonsActive){
                encryptButtonOutlet.isEnabled = false;
                decryptButtonOutlet.isEnabled = false;
                buttonsActive = false;
        };
        if ( (inputTextBox.stringValue.count > 0 || inputKeyTextBox.stringValue.count > 0) && !buttonsActive){
            encryptButtonOutlet.isEnabled = true;
            decryptButtonOutlet.isEnabled = true;
            buttonsActive = true;
        };
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTextBox.delegate = self;
        let model = EncryptorModel.init();
        self.controller = EncryptionController.init(m: model, vc: self);
        self.model = model;
    }

    override var representedObject: Any? {
        didSet {
            
        }
    }
}

